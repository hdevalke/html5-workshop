/*jslint browser:true, devel:true, todo:true */
/*global WebSocket, KeyboardEvent, FileReader, Notification, ArrayBuffer,Blob, webkitURL, Int32Array, Uint16Array, Int8Array, webkitIndexedDB, indexedDB,webkitIDBTransaction, google*/

// Example for binary data
//var ws = new WebSocket('ws://localhost:8080/html5-workshop/chat-server');
//ws.binaryType = 'arraybuffer';
//ws.onmessage = function (msg){
//  console.log(msg);
//  if(msg.data instanceof ArrayBuffer) {
//    var userlength = new Int32Array(msg.data,0,4)[0];
//    var userstring = new Uint16Array(msg.data,4,userlength/2); // 32/16 = 2
//    var data = new Int8Array(msg.data,4+userlength);
//    console.log(String.fromCharCode.apply(null,userstring));
//    console.log(userstring);
//    console.log(data);
//  }
//}
/**
 * this function can help to determine what kind of arguments are passed to
 * a callback function.
 */
function logArguments(prefix) {
    'use strict';
    return function () {
        var x;
        console.log(prefix);
        console.log('this:');
        console.log(this);
        console.log('arguments:');
        for (x in arguments) {
            if (arguments.hasOwnProperty(x)) {
                console.log(arguments[x]);
            }
        }
    };
}
Date.prototype.format = function (fmt) {
    'use strict';
    // based on http://snipplr.com/view.php?codeview&id=66968
    var replacers = {d: ['getDate', function (v) { return ('0' + v).substr(-2, 2); }],
            m: ['getMonth', function (v) { return ('0' + v).substr(-2, 2); }],
            y: ['getFullYear'],
            H: ['getHours', function (v) { return ('0' + v).substr(-2, 2); }],
            M: ['getMinutes', function (v) { return ('0' + v).substr(-2, 2); }],
            S: ['getSeconds', function (v) { return ('0' + v).substr(-2, 2); }]},
        date = this,
        dateStr = fmt.replace(/([dmyHMS])/g, function (match) {
            var newValue = date[(replacers[match])[0]]();
            if (replacers[match][1]) {
                newValue = replacers[match][1](newValue);
            }
            return newValue;
        });

    return dateStr;
};
var chat = (function (indexedDB, URL) {
    'use strict';
    var chat = {},
        ENTER = 13,
        ESC = 27,
        templates,
        user,
        socket,
        db;
    function showNotification(msg) {
        if (!msg.user.equals(user)) {
            var n;
            if (msg instanceof chat.msg.Text) {
                n = new Notification(msg.user.name + ':\n' + msg.message);
            } else if (msg instanceof chat.msg.SignIn) {
                n = new Notification(msg.user.name + ' has signed in.');
            } else if (msg instanceof chat.msg.SignOut) {
                n = new Notification(msg.user.name + 'has signed out');
            }
            n.onshow = function () {
                setTimeout(function () {n.close(); }, 3000);
            };
            n.show();
        }
    }
    function connect(url) {
        socket = new WebSocket('ws://localhost:8080/html5/chat-server');
        socket.binaryType = 'arraybuffer';
        socket.onopen = function (event) {
            var msg = new chat.msg.SignIn(user);
            socket.send(msg.toJson());
        };
        socket.onclose = function (event) {
            var msg = new chat.msg.SignOut();
            socket.send(msg);
        };
        socket.onmessage = function (event) {
            var msg, offset = 0, userlength, userstring, filenameLength, filename,
                filetypeLength, filetype, data, request, sharedFile, user;
            if (event.data instanceof ArrayBuffer) {
                // user
                userlength = new Uint16Array(event.data, offset, 2);
                userlength = userlength[0] | (userlength[1] << 16);
                offset += 4;
                userstring = new Uint16Array(event.data, offset, userlength / 2); // 32/16 = 2
                offset += userlength;
                // file name
                filenameLength = new Uint16Array(event.data, offset, 2);
                filenameLength = filenameLength[0] | (filenameLength[1] << 16);
                offset += 4;
                filename = new Uint16Array(event.data, offset, filenameLength / 2);
                offset += filenameLength;
                // file type
                filetypeLength = new Uint16Array(event.data, offset, 2);
                filetypeLength = filetypeLength[0] | (filetypeLength[1] << 16);
                offset += 4;
                filetype = new Uint16Array(event.data, offset, filetypeLength / 2);
                offset += filetypeLength;
                // data
                data = new Int8Array(event.data, offset);
                //
                user = JSON.parse(String.fromCharCode.apply(null, userstring), chat.msg.json.reviver);
                filename = String.fromCharCode.apply(null, filename);
                filetype = String.fromCharCode.apply(null, filetype);
                sharedFile = new SharedFile(user, filename, filetype, data);
                msg = new chat.msg.Message('file', user);
                msg.data = '<a href="' + URL.createObjectURL(sharedFile.blob)
                    + '" download="' + sharedFile.filename + '">' + sharedFile.filename + '</a>';
                chat.insertMessage(msg);
            } else {
                msg = chat.msg.Message.fromJson(event.data);
                chat.insertMessage(msg);
                showNotification(msg);
                addMessageToDB(msg);
            }
        };
    }
    function initDb() {
        var request = indexedDB.open('messages');
        request.onsuccess = function (e) {
            db = request.result;
            var version = '1.0';
            if (version !== db.version) {
                db.setVersion(version).onsuccess = function (e) {
                    //db.deleteObjectStore('messages');
                    var store = db.createObjectStore('messages', {keyPath: 'id', autoIncrement: true});
                };
            }
            // load all messages
            db.transaction(['messages'], 'readwrite')
                    .objectStore('messages').openCursor().onsuccess = function (e) {
                    var cursor = e.target.result, msg;
                    if (cursor) {
                        console.log(cursor);
                        msg = chat.msg.Message.fromJson(JSON.stringify(cursor.value));
                        chat.insertMessage(msg);
                        cursor.continue();
                    }
                };
            // add a keylistener to clear the db.
            window.addEventListener('keyup', function (e) {
                if (e.keyCode === ESC) {
                    db.transaction(['messages'], 'readwrite').objectStore('messages').clear();
                }
            });
        };
    }
    function addMessageToDB(message) {
        if (db) { // db must be defined yet
            var t = db.transaction(['messages'], 'readwrite'),
                store = t.objectStore('messages');
            store.put(message);
        }
    }
    // Types
    /**
     * A chat user.
     * @constructor
     * @param {String} name The name of the user.
     * @param {String} location The location of the user.
     */
    chat.User = function (name, location) {
        this.name = name;
        this.location = location;
        this.pictureDataUrl = undefined;
    };
    chat.User.prototype.equals = function (user) {
        return (this.name === user.name) && (this.location === user.location);
    };
    chat.msg = {};
    /**
     * An abstract chat message.
     * @constructor
     * @param {String} type To tag the messages type.
     * @param {chat.User} user The user owning the message.
     */
    chat.msg.Message = function (type, user) {
        this.type = type;
        this.date = new Date();
        this.user = user;
    };

    chat.msg.Message.prototype.toJson = function () {
        return JSON.stringify(this, chat.msg.json.replacer);
    };
    /**
     * Takes a json string and creates an correct instance of the message.
     * @param json {String} the string version of the message.
     */
    chat.msg.Message.fromJson = function (json) {
        var object = JSON.parse(json, chat.msg.json.reviver),
            o,
            x;
        // create the new object
        switch (object.type) {
        case 'text':
            o = new chat.msg.Text();
            break;
        case 'picture':
            o = new chat.msg.Picture();
            break;
        case 'signin':
            o = new chat.msg.SignIn();
            break;
        case 'signout':
            o = new chat.msg.SignOut();
            break;
        }
        // copy the members
        /*jslint forin:true*/
        for (x in object) {
            o[x] = object[x];
        }
        return o;
    };
    function SharedFile(user, filename, type, data) {
        this.date = new Date();
        this.user = user;
        this.filename = filename;
        this.blob = new Blob([data], {type:type});
    }
    /**
     * A sign in message
     * @constructor
     * @augments chat.msg.Message
     */
    chat.msg.SignIn = function (user) {
        chat.msg.Message.call(this, 'signin', user);
    };
    chat.msg.SignIn.prototype = Object.create(chat.msg.Message.prototype);
    /**
     * A sign out message
     * @constructor
     * @augments chat.msg.Message
     */
    chat.msg.SignOut = function (user) {
        chat.msg.Message.call(this, 'signout', user);
    };
    chat.msg.SignOut.prototype = Object.create(chat.msg.Message.prototype);
    /**
     * A text message.
     * @constructor
     * @augments chat.msg.Message
     * @param {String} message The text message.
     * @param {User} user The owner of the message.
     */
    chat.msg.Text = function (message, user) {
        chat.msg.Message.call(this, 'text', user);
        this.data = message || '';
    };

    chat.msg.Text.prototype = Object.create(chat.msg.Message.prototype, {
        message : {
            get: function () {
                return this.data;
            },
            set: function (message) {
                this.data = message;
            }
        }
    });
    chat.msg.Text.prototype.constructor = chat.msg.Text;
    /**
     * A picture message. Used to update profile picture.
     * @constructor
     * @augments chat.msg.Message
     * @param {String} dataUrl the dataUrl of the picture.
     */
    chat.msg.Picture = function (dataUrl) {
        chat.msg.Message.call(this, 'picture');
        this.data = dataUrl;
    };
    chat.msg.Picture.prototype = Object.create(chat.msg.Message.prototype, {
        dataUrl : {
            get: function () {
                return this.data;
            },
            set: function (dataUrl) {
                this.data = dataUrl;
            }
        }
    });
    chat.msg.Picture.prototype.constructor = chat.msg.Picture;
    // json helpers
    chat.msg.json = {
        /**
         * Transforms milliseconds to dates and object members to there object type.
         * @param key The key of the property
         * @param value The value of the property
         */
        reviver: function (key, value) {
            var user;
            switch (key) {
            case 'user':
                user = new chat.User(value.name, value.location);
                user.pictureDataUrl = value.pictureDataUrl;
                return user;
            case 'date':
                return new Date(value);
            default:
                return value;
            }
        },
        /**
         * Makes a copy of the message and replaces the date by milliseconds.
         * @param key The key of the property
         * @param value The value of the property
         * @return {undefined, Object}
         */
        replacer: function (key, value) {
            var x, newValue;

            if (key === '') {
                newValue = {};
                for (x in value) {
                    if (value.hasOwnProperty(x)) {
                        if (value[x] instanceof Date) {
                            newValue[x] = value[x].getTime();// date to milliseconds
                        } else {
                            newValue[x] = value[x];
                        }
                    }
                }
                return newValue;
            }
            return value;
        }
    };
    // helper functions
    /**
     * @param {CanvasRenderingContext2D} ctx the canvas 2d context
     * @param {HTMLImage} image The image to resize
     * @param {Number} x x position to start drawing
     * @param {Number} y y position to start drawing
     * @param {Number} width max width of the image
     * @param {Number} height max height of the image
     */
    function drawAspectResize(ctx, image, x, y, width, height) {
        height = height || width;
        var aw = width / image.width,
            ah = height / image.height,
            scale = Math.min(aw, ah),
            nw = image.width * scale,
            nh = image.height * scale,
            ox = nw < nh ? (width - nw) / 2 : 0,
            oy = nw > nh ? (height - nh) / 2 : 0;
        ctx.clearRect(x, y, width, height);
        ctx.drawImage(image, x + ox, y + oy, nw, nh);
    }
    /**
     * check if something has to be sent to the server
     * @param {Event} e the event object
     */
    function send(e) {
        if (e instanceof KeyboardEvent && e.keyCode === ENTER && !e.shiftKey) {
            var message = e.target.value,
                text = new chat.msg.Text(message, user);
            socket.send(text.toJson());
            e.target.value = '';
        }
    }
    // init functions for sign in
    function initPicture() {
        var ctx = document.querySelector('#picture').getContext('2d'),
            img = new Image(),
            msg;
        img.src = 'images/person.png';
        img.onload = function () {
            drawAspectResize(ctx, img, 0, 0, 32);
            if (chat.defaultPicture) {
                user.pictureDataUrl =  ctx.canvas.toDataURL();
                msg = new chat.msg.Picture(user.pictureDataUrl);
                socket.send(msg.toJson());
            } else {
                chat.defaultPicture = ctx.canvas.toDataURL();
            }
        };
        ctx.canvas.addEventListener('dragenter', function (e) {
            e.target.style.border = '1px dashed grey';
        });
        ctx.canvas.addEventListener('dragleave', function (e) {
            e.target.style.border = 'none';
        });
        ctx.canvas.addEventListener('drop', function (e) {
            var file, reader;
            e.target.style.border = 'none';
            if (e.dataTransfer.files) {
                file = e.dataTransfer.files[0];
                if (/image\/(?:jpe?g|gif|png)/.test(file.type)) {
                    reader = new FileReader();
                    reader.onload = function (evt) {
                        img.src = evt.target.result;
                    };
                    reader.readAsDataURL(file);
                } else {
                    console.log('wrong file type dropped');
                }
            }
            e.preventDefault();
        });
    }

    function str2ab(str) {
        var buf = new ArrayBuffer(str.length * 2), // 2 bytes for each char
            bufView = new Uint16Array(buf),
            i,
            n;
        for (i = 0, n = str.length; i < n; i += 1) {
            bufView[i] = str.charCodeAt(i);
        }
        return buf;
    }
    /**
     * @param {String} username The user to sign in.
     */
    function initSignedIn(username) {
        user = new chat.User(username);
        // fetch the users location
        navigator.geolocation.getCurrentPosition(function (pos) {
            var latLng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude),
                geocoder = new google.maps.Geocoder();

            geocoder.geocode({latLng: latLng}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    user.location = results[1].formatted_address;
                } else {
                    console.error('google.maps.Geocoder failed due to: ' + status);
                }
                initDb();
                connect();
            });
        });
        initPicture();
        var input = document.querySelector('#input'), x;
        input.addEventListener('keyup', send);
        input.addEventListener('dragenter', function (e) {
            e.target.style.border = '1px dashed grey';
        });
        input.addEventListener('dragleave', function (e) {
            e.target.style.border = 'none';
        });
        input.addEventListener('drop', function (e) {
            e.target.style.border = 'none';
            console.log(e.dataTransfer.files);
            if (e.dataTransfer.files) {
                Array.prototype.forEach.call(e.dataTransfer.files, function (file) {
                    console.log(file);
                    var nameBuf = str2ab(file.name),
                        typeBuf = str2ab(file.type),
                        nameSize = nameBuf.byteLength,
                        typeSize = typeBuf.byteLength,
                        dataSize = file.size,
                        buf = new ArrayBuffer(4 + nameSize + 4 + typeSize + dataSize),
                        offset = 0,
                        bufView;
                    // filename
                    bufView = new Uint16Array(buf, offset, 2);
                    bufView[1] = (nameSize>>16);
                    bufView[0] = nameSize;
                    offset += 4;
                    bufView = new Uint16Array(buf, offset, nameSize / 2);
                    bufView.set(new Uint16Array(nameBuf));
                    offset += nameSize;
                    // filetype
                    bufView = new Uint16Array(buf, offset,2);
                    bufView[1] = (typeSize>>16);
                    bufView[0] = typeSize;
                    offset += 4;
                    bufView = new Uint16Array(buf, offset, typeSize / 2);
                    bufView.set(new Uint16Array(typeBuf));
                    offset += typeSize;
                    //data
                    bufView = new Int8Array(buf, offset);
                    var reader = new FileReader(file);
                    reader.onload = function (e) {
                        bufView.set(new Int8Array(e.target.result));
                        socket.send(buf);
                    };
                    reader.readAsArrayBuffer(file);
                });
            }
            e.preventDefault();
        });
    }

    /**
     * Checks the username input field and starts the chat session.
     * @param {Event} e the event that triggered the sign in.
     * @param {HTMLInputElement} input the input element with the user name.
     */
    function signIn(e, input) {
        if (input.validity.patternMismatch) {
            input.setCustomValidity(input.validationMessage + ' ' + input.pattern);
            return;
        }
        input.setCustomValidity('');

        var signedOut = document.querySelector('#signed-out'),
            signedIn = document.querySelector('#signed-in');

        signedIn.classList.add('front');
        signedOut.classList.add('back');
        signedIn.classList.remove('back');
        signedOut.classList.remove('front');
        initSignedIn(input.value);
    }

    function initSingedOut() {
        var signedOut = document.querySelector('#signed-out'),
            button = signedOut.querySelector('button'),
            input = signedOut.querySelector('input');
        signedOut.querySelector('form').onsubmit = function () {return false; };
        button.addEventListener('click', function (e) {signIn(e, input); });
    }
    /**
     * Initializes the chat application.
     * This method should be called when the page is loaded.
     */
    function init() {
        initSingedOut();
    }
    // initialize on window load.
    window.addEventListener('load', function () {
        init();
    });
    /**
     * @param {chat.msg.Message} message
     */
    chat.insertMessage = function (message) {
        var messagesDiv = document.querySelector('#messages'),
            div;
        if (message instanceof chat.msg.Text) {
            div = templates.text(message);
        } else {
            div = templates.info(message);
        }
        messagesDiv.appendChild(div);
        messagesDiv.scrollTop = messagesDiv.scrollHeight;
    };
    // templates
    templates = {
        /**
         * Creates a new text message view.
         * @param {chat.msg.Text} message The text message.
         */
        text : function (message) {
            var div = document.createElement('div');
            div.classList.add('message');
            div.innerHTML = '<img width="32" height="32" src="' + (message.user.pictureDataUrl || chat.defaultPicture) + '">' +
                            '<section class="user-info"><span>' + message.user.name + '</span>,' +
                            '  <span>' + (message.user.location || '') + '</span><br>' +
                            '  <span>' + message.date.format('H:M:S') + '</span>' +
                            '</section>' +
                            '<p>' + (message.message || '') + '</p>';
            return div;
        },
        /**
         * Creates a new text message view.
         * @param {chat.msg.Message} message
         */
        info : function (message) {
            if (message instanceof chat.msg.SignIn) {
                message.data = message.user.name + ' has signed in.';
            }
            if (message instanceof chat.msg.SignOut) {
                message.data = message.user.name + ' has signed out';
            }
            var div = document.createElement('div');
            div.classList.add('message');
            div.classList.add('info');
            div.innerHTML = '<p>' + (message.data || '') + '</p>';
            return div;
        }
    };

    return chat;
}(webkitIndexedDB, webkitURL));

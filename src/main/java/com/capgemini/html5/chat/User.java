/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.capgemini.html5.chat;

/**
 *
 * @author HDEVALKE
 */
public class User {
    String name;
    String location;
    String pictureDataUrl;
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPictureDataUrl() {
        return pictureDataUrl;
    }

    public void setPictureDataUrl(String pictureDataUrl) {
        this.pictureDataUrl = pictureDataUrl;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.capgemini.html5.chat.message;

import com.capgemini.html5.chat.User;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Date;

/**
 * Abstract class for messages. A date and user can be assigned to the message.
 * Any subtype sho
 * @author HDEVALKE
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
    @JsonSubTypes.Type(value = Text.class, name = "text"),
    @JsonSubTypes.Type(value = Picture.class, name="picture"),
    @JsonSubTypes.Type(value = SignIn.class, name="signin"),
    @JsonSubTypes.Type(value = SignOut.class, name="signout")
})
public abstract class Message<T>{

    T data;
    Date date;
    User user;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

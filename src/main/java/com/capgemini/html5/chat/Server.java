/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.capgemini.html5.chat;

import com.capgemini.html5.chat.message.Message;
import com.capgemini.html5.chat.message.Picture;
import com.capgemini.html5.chat.message.SignIn;
import com.capgemini.html5.chat.message.SignOut;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.StreamInbound;
import org.apache.catalina.websocket.WebSocketServlet;
import org.apache.catalina.websocket.WsOutbound;

/**
 *
 * @author HDEVALKE
 */
@WebServlet(urlPatterns = "/chat-server", asyncSupported = true)
public class Server extends WebSocketServlet {
    
    final static Logger log = Logger.getLogger(Server.class.getName());
    static final long serialVersionUID = 1L;
    static final String GUEST_PREFIX = "Guest";
    final ObjectMapper mapper = new ObjectMapper();
    final AtomicInteger connectionIds = new AtomicInteger(0);
    final Set<ChatMessageInbound> connections =
            new CopyOnWriteArraySet<ChatMessageInbound>();
    
    {
        // configure mapper
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }
    
    @Override
    protected StreamInbound createWebSocketInbound(String subProtocol,
            HttpServletRequest request) {
        return new ChatMessageInbound();
    }
    
    private final class ChatMessageInbound extends MessageInbound {
        
        User user = new User();
        
        @Override
        protected void onOpen(WsOutbound outbound) {
            connections.add(this);
            log.info("ChatMessageInbound.onOpen: new connection added");
        }
        
        @Override
        protected void onClose(int status) {
            try {
                connections.remove(this);
                log.info(String.format("ChatMessageInbound.onClose: user (%s) signed out", user.getName()));
                SignOut signOut = new SignOut();
                signOut.setUser(user);
                broadcast(mapper.writeValueAsString(signOut));
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        @Override
        /**
         * Receives a binary message. The structure is as follows: | length of
         * mime type (4 bytes)] | mime type | data | Broadcasts a message to the
         * other users with the following structure: | lenght of user json (4
         * bytes) | user json | length of mime type (4 bytes)] | mime type |
         * data |
         */
        protected void onBinaryMessage(ByteBuffer message) throws IOException {
            // "UTF-16LE" to easily convert to javascript string
            byte[] userBytes = mapper.writeValueAsString(user).getBytes("UTF-16LE");
            // number of userbytes to 
            int userLength = userBytes.length;
            // total length: user lenght int + user bytes + data bytes 
            int length = userBytes.length + Integer.SIZE / Byte.SIZE + message.limit();
            ByteBuffer response = ByteBuffer.allocate(length);
            response.order(ByteOrder.LITTLE_ENDIAN);
            response.putInt(userLength);
            response.put(userBytes);
            response.put(message);
            broadcast(response);
        }
        
        @Override
        protected void onTextMessage(CharBuffer message) throws IOException {
            log.info(String.format("ChatMessageInbound.onTextMessage: %s", message));
            try {
                Message m = mapper.readValue(message.toString(), Message.class);
                if (m instanceof SignIn) {
                    this.user = m.getUser();
                } else if (m instanceof SignOut) {
                    m.setUser(user);
                    this.getWsOutbound().close(0, null);   
                } else if (m instanceof Picture) {
                    Picture p = (Picture) m;
                    user.pictureDataUrl = p.getDataUrl();
                } else {
                    m.setUser(user);
                }
                broadcast(mapper.writeValueAsString(m));
            } catch (IOException e) {
                log.severe(e.getMessage());
            }
            
        }
        
        private void broadcast(ByteBuffer buffer) {
            for (ChatMessageInbound connection : connections) {
                try {
                    connection.getWsOutbound().writeBinaryMessage(buffer);
                } catch (IOException ignore) {
                    // Ignore
                    log.log(Level.SEVERE, ignore.getMessage(), ignore);
                }
            }
        }
        
        private void broadcast(String message) {
            for (ChatMessageInbound connection : connections) {
                try {
                    CharBuffer buffer = CharBuffer.wrap(message);
                    connection.getWsOutbound().writeTextMessage(buffer);
                } catch (IOException ignore) {
                    // Ignore
                }
            }
        }
    }
}
